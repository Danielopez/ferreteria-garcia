/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        light: {
          ...require("daisyui/src/theming/themes")["light"],
          primary: "#1d4ed8",
          secondary: "#0ea5e9",
          accent: "#84cc16",
          neutral: "#1f2937",
          "base-100": "#ffffff",
          info: "#2563eb",
          success: "#15803d",
          warning: "#f59e0b",
          error: "#dc2626",
          "--rounded-box": "0",
          "--rounded-btn": "0",
        },
      },
    ],
  },
};
