export function fixedNumber(number: number, n: number = 2): number {
  if (number) {
    const fixed = number.toFixed(n);
    return Number(fixed);
  }
  return 0;
}

export function formatDate(date: Date | undefined): string {
  if (!date) {
    return 'Sin fecha';
  }
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const year = date.getFullYear();

  return `${day}/${month}/${year}`;
}

export function getCurrentDate(): string {
  const fecha = new Date();
  const day = fecha.getDate().toString().padStart(2, '0');
  const month = (fecha.getMonth() + 1).toString().padStart(2, '0');
  const year = fecha.getFullYear();
  const hour = fecha.getHours().toString().padStart(2, '0');
  const minutes = fecha.getMinutes().toString().padStart(2, '0');
  const seconds = fecha.getSeconds().toString().padStart(2, '0');

  return ` [${day}-${month}-${year}T${hour}-${minutes}-${seconds}]`;
}
