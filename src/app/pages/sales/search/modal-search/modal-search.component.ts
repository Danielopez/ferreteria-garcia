import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Output,
  ViewChild,
} from '@angular/core';
import { ModalService } from '../../../../core/services/modals/modal.service';
import { Modals } from '../../../../core/enums/modals.enum';
import { IconComponent } from '../../../../shared/icon/icon.component';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs';
import { DbService } from '../../../../core/services/database/db.service';
import { Product } from '../../../../core/services/database/class/product.class';
import { ProductsTableComponent } from './products-table/products-table.component';
import { AlertComponent } from '../../../../shared/alert/alert.component';

@Component({
  selector: 'app-modal-search',
  standalone: true,
  imports: [
    IconComponent,
    ReactiveFormsModule,
    ProductsTableComponent,
    AlertComponent,
  ],
  templateUrl: './modal-search.component.html',
  styleUrl: './modal-search.component.css',
})
export class ModalSearchComponent {
  @ViewChild('searchInput') searchInput!: ElementRef<HTMLInputElement>;
  @Output()
  productSelected = new EventEmitter<Product>();
  open: boolean = false;
  search = new FormControl('');
  products: Product[] | undefined;
  index: number = 0;
  constructor(private modalService: ModalService, private db: DbService) {}
  ngOnInit(): void {
    this.modalService.modalsBO.subscribe((data) => {
      this.open = data[Modals.SALE_SEACH];
      if (this.open) {
        setTimeout(() => {
          this.searchInput.nativeElement.focus();
          this.searchInput.nativeElement.select();
        }, 0);
      }
    });
    this.search.valueChanges
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe((query) => {
        if (query) {
          this.db.searchProducts(query).subscribe((data) => {
            this.products = data;
            this.index = 0;
          });
        }
      });
  }
  closeModal() {
    // this.products = undefined;
    // this.search.reset();
    this.modalService.closeModal(Modals.SALE_SEACH);
  }
  clickProduct(index: number) {
    if (this.products) {
      const product = this.products[index];
      if (product) {
        this.productSelected.emit(product);
        this.closeModal();
      }
    }
  }
  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Escape' && this.open) {
      this.closeModal();
    }
    if (this.products && this.products.length > 0) {
      if (event.key === 'ArrowUp' && this.open) {
        if (this.index > 0) {
          this.index--;
        }
        setTimeout(() => {
          const inputElement = this.searchInput.nativeElement;
          inputElement.setSelectionRange(
            inputElement.value.length,
            inputElement.value.length
          );
        }, 0);
      } else if (event.key === 'ArrowDown' && this.open) {
        if (this.index < this.products.length - 1) {
          this.index++;
        }
      } else if (event.key === 'Enter' && this.open) {
        const product = this.products[this.index];
        if (product) {
          this.productSelected.emit(product);
          this.closeModal();
        }
      }
    }
  }
}
