import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { Product } from '../../../../../core/services/database/class/product.class';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-products-table',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './products-table.component.html',
  styleUrl: './products-table.component.css',
})
export class ProductsTableComponent {
  @ViewChildren('productRow') productRows!: QueryList<ElementRef>;
  @Input()
  products: Product[] | undefined;
  @Input()
  index: number = 0;
  @Output()
  clickProductEvent = new EventEmitter<number>();

  ngOnChanges(changes: SimpleChanges) {
    if (changes['index']) {
      this.scrollToHighlighted();
    }
  }

  clickProduct(index: number) {
    this.clickProductEvent.emit(index);
  }

  scrollToHighlighted() {
    if (this.products && this.productRows) {
      const highlightedRow = this.productRows.toArray()[this.index];
      if (highlightedRow) {
        highlightedRow.nativeElement.scrollIntoView({
          block: 'center',
        });
      }
    }
  }
}
