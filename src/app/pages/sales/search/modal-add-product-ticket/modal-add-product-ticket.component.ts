import {
  Component,
  ElementRef,
  HostListener,
  Input,
  ViewChild,
} from '@angular/core';
import { Product } from '../../../../core/services/database/class/product.class';
import { ModalService } from '../../../../core/services/modals/modal.service';
import { Modals } from '../../../../core/enums/modals.enum';
import { ProductTicket } from '../../../../core/class/product-ticket.class';
import { IconComponent } from '../../../../shared/icon/icon.component';
import { FormsModule } from '@angular/forms';
import { fixedNumber, formatDate } from '../../../../utils/utils';
import { TicketService } from '../../../../core/services/ticket/ticket.service';

@Component({
  selector: 'app-modal-add-product-ticket',
  standalone: true,
  imports: [IconComponent, FormsModule],
  templateUrl: './modal-add-product-ticket.component.html',
  styleUrl: './modal-add-product-ticket.component.css',
})
export class ModalAddProductTicketComponent {
  @ViewChild('priceInput') priceInput!: ElementRef<HTMLInputElement>;
  @ViewChild('quantityInput') quantityInput!: ElementRef<HTMLInputElement>;
  @Input()
  product: Product | undefined;

  productTicket: ProductTicket | undefined;
  open: boolean = false;
  price: number = 0;

  constructor(
    private modalService: ModalService,
    private ticketService: TicketService
  ) {}

  ngOnInit(): void {
    this.modalService.modalsBO.subscribe((data) => {
      this.open = data[Modals.ADD_PRODUCT_TICKET];
      if (this.open) {
        if (this.product) {
          this.productTicket = new ProductTicket(this.product);
          setTimeout(() => {
            this.quantityInput.nativeElement.focus();
            this.quantityInput.nativeElement.select();
          }, 0);
        }
      }
    });
  }
  get update() {
    const date = formatDate(this.product?.UPDATE);
    return date;
  }
  get total() {
    if (this.productTicket) {
      return fixedNumber(
        this.productTicket.price * this.productTicket.quantity
      );
    }
    return 0;
  }
  changeTypePrice() {
    if (this.productTicket) {
      switch (this.productTicket.typePrice) {
        case 0:
          setTimeout(() => {
            this.priceInput.nativeElement.focus();
            this.priceInput.nativeElement.select();
          }, 0);
          break;
        default:
          this.quantityInput.nativeElement.focus();
          break;
      }
      this.setPrice();
    }
  }
  setPrice() {
    if (this.productTicket) {
      let price: number;
      switch (this.productTicket.typePrice) {
        case 0:
          price = fixedNumber(this.price);
          this.productTicket.setPrice(price);
          break;
        case 1:
          price = this.productTicket.product.PRECIO1 || 0;
          this.productTicket.setPrice(price);
          break;
        case 2:
          price = this.productTicket.product.PRECIO2 || 0;
          this.productTicket.setPrice(price);
          break;
        case 3:
          price = this.productTicket.product.PRECIO3 || 0;
          this.productTicket.setPrice(price);
          break;
        default:
          break;
      }
    }
  }
  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Enter' && this.open) {
      this.addProduct();
    }
    if (event.key === 'Escape' && this.open) {
      this.closeModal();
    }
  }
  addProduct() {
    if (this.productTicket && this.productTicket.price > 0) {
      const newProduct = new ProductTicket(this.productTicket.product);
      newProduct.price = this.productTicket.price;
      newProduct.quantity = this.productTicket.quantity;
      newProduct.typePrice = this.productTicket.typePrice;
      this.ticketService.addProduct(newProduct);
      this.closeModal();
    }
  }
  closeModal() {
    this.price = 0;
    this.modalService.closeModal(Modals.ADD_PRODUCT_TICKET);
  }
}
