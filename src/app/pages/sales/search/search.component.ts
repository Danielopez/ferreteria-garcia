import { Component } from '@angular/core';
import { IconComponent } from '../../../shared/icon/icon.component';
import { ModalSearchComponent } from './modal-search/modal-search.component';
import { ModalService } from '../../../core/services/modals/modal.service';
import { Modals } from '../../../core/enums/modals.enum';
import { Product } from '../../../core/services/database/class/product.class';
import { ModalAddProductTicketComponent } from './modal-add-product-ticket/modal-add-product-ticket.component';

@Component({
  selector: 'app-search',
  standalone: true,
  imports: [
    IconComponent,
    ModalSearchComponent,
    ModalAddProductTicketComponent,
  ],
  templateUrl: './search.component.html',
  styleUrl: './search.component.css',
})
export class SearchComponent {
  productSelected: Product | undefined;
  constructor(private modalService: ModalService) {}
  openModalSearchProduct() {
    this.modalService.openModal(Modals.SALE_SEACH);
  }
  openModalAddProduct(product: Product) {
    this.productSelected = product;
    setTimeout(() => {
      this.modalService.openModal(Modals.ADD_PRODUCT_TICKET);
    }, 0);
  }
}
