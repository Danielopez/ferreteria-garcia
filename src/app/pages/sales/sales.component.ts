import { Component } from '@angular/core';
import { SearchComponent } from './search/search.component';
import { TicketComponent } from './ticket/ticket.component';

@Component({
  selector: 'app-sales',
  standalone: true,
  imports: [SearchComponent, TicketComponent],
  templateUrl: './sales.component.html',
  styleUrl: './sales.component.css',
})
export class SalesComponent {}
