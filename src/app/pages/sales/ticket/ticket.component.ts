import { Component } from '@angular/core';
import { TicketService } from '../../../core/services/ticket/ticket.service';
import { Ticket } from '../../../core/services/ticket/class/ticket.class';
import { fixedNumber } from '../../../utils/utils';
import { ProductTicket } from '../../../core/class/product-ticket.class';
import { IconComponent } from '../../../shared/icon/icon.component';
import { ModalService } from '../../../core/services/modals/modal.service';
import { Modals } from '../../../core/enums/modals.enum';
import { EditProductModalComponent } from './edit-product-modal/edit-product-modal.component';

@Component({
  selector: 'app-ticket',
  standalone: true,
  imports: [IconComponent, EditProductModalComponent],
  templateUrl: './ticket.component.html',
  styleUrl: './ticket.component.css',
})
export class TicketComponent {
  ticket: Ticket | undefined;
  productSelected: ProductTicket | undefined;
  indexSelected: number = 0;
  constructor(
    private ticketService: TicketService,
    private modalService: ModalService
  ) {}
  ngOnInit(): void {
    this.ticketService.ticketBO.subscribe((data) => {
      this.ticket = data;
    });
  }
  get total() {
    if (this.ticket && this.ticket.products.length > 0) {
      let total: number = 0;
      this.ticket.products.map((product) => {
        total = total + product.price * product.quantity;
      });
      return fixedNumber(total);
    }
    return 0;
  }
  editProduct(index: number) {
    this.productSelected = this.ticket?.products[index];
    this.indexSelected = index;
    setTimeout(() => {
      this.modalService.openModal(Modals.EDIT_PRODUCT_TICKET);
    }, 0);
  }
  fixedDigits(number: number) {
    return fixedNumber(number);
  }
  clearTicket() {
    if (this.ticket && confirm('¿Esta seguro de borrar todo el ticket?')) {
      this.ticket.resetTicket();
    }
  }
}
