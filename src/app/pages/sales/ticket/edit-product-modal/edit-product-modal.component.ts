import {
  Component,
  ElementRef,
  HostListener,
  Input,
  ViewChild,
} from '@angular/core';
import { ProductTicket } from '../../../../core/class/product-ticket.class';
import { ModalService } from '../../../../core/services/modals/modal.service';
import { TicketService } from '../../../../core/services/ticket/ticket.service';
import { Modals } from '../../../../core/enums/modals.enum';
import { fixedNumber, formatDate } from '../../../../utils/utils';
import { IconComponent } from '../../../../shared/icon/icon.component';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-edit-product-modal',
  standalone: true,
  imports: [IconComponent, FormsModule],
  templateUrl: './edit-product-modal.component.html',
  styleUrl: './edit-product-modal.component.css',
})
export class EditProductModalComponent {
  @ViewChild('priceInput') priceInput!: ElementRef<HTMLInputElement>;
  @ViewChild('quantityInput') quantityInput!: ElementRef<HTMLInputElement>;

  @Input()
  productTicket: ProductTicket | undefined;
  @Input()
  index: number = 0;

  productTmp: ProductTicket | undefined;
  open: boolean = false;
  price: number = 0;

  constructor(
    private modalService: ModalService,
    private ticketService: TicketService
  ) {}

  ngOnInit(): void {
    this.modalService.modalsBO.subscribe((data) => {
      this.open = data[Modals.EDIT_PRODUCT_TICKET];
      if (this.open) {
        if (this.productTicket) {
          this.productTmp = new ProductTicket(this.productTicket.product);
          this.productTmp.price = this.productTicket.price;
          this.productTmp.quantity = this.productTicket.quantity;
          this.productTmp.typePrice = this.productTicket.typePrice;
          this.price = this.productTmp.price;
          setTimeout(() => {
            this.quantityInput.nativeElement.focus();
            this.quantityInput.nativeElement.select();
          }, 0);
        }
      }
    });
  }
  get update() {
    const date = formatDate(this.productTmp?.product?.UPDATE);
    return date;
  }
  get total() {
    if (this.productTmp) {
      return fixedNumber(this.productTmp.price * this.productTmp.quantity);
    }
    return 0;
  }
  changeTypePrice() {
    if (this.productTmp) {
      switch (this.productTmp.typePrice) {
        case 0:
          setTimeout(() => {
            this.priceInput.nativeElement.focus();
            this.priceInput.nativeElement.select();
          }, 0);
          break;
        default:
          this.quantityInput.nativeElement.focus();
          break;
      }
      this.setPrice();
    }
  }
  setPrice() {
    if (this.productTmp) {
      let price: number;
      switch (this.productTmp.typePrice) {
        case 0:
          price = fixedNumber(this.price);
          this.productTmp.setPrice(price);
          break;
        case 1:
          price = this.productTmp.product.PRECIO1 || 0;
          this.productTmp.setPrice(price);
          break;
        case 2:
          price = this.productTmp.product.PRECIO2 || 0;
          this.productTmp.setPrice(price);
          break;
        case 3:
          price = this.productTmp.product.PRECIO3 || 0;
          this.productTmp.setPrice(price);
          break;
        default:
          break;
      }
    }
  }
  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Escape' && this.open) {
      this.closeModal();
    }
    if (event.key === 'Enter' && this.open) {
      if (this.productTicket && this.productTmp) {
        this.updateProduct();
      }
    }
  }
  updateProduct() {
    if (this.productTicket && this.productTmp && this.productTmp.price > 0) {
      this.productTicket.price = this.productTmp.price;
      this.productTicket.quantity = this.productTmp.quantity;
      this.productTicket.typePrice = this.productTmp.typePrice;
      this.closeModal();
    }
  }
  deleteProductTicket() {
    this.ticketService.deleteProduct(this.index);
    this.closeModal();
  }
  closeModal() {
    this.price = 0;
    this.modalService.closeModal(Modals.EDIT_PRODUCT_TICKET);
  }
}
