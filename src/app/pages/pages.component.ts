import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InitDatabaseModalComponent } from '../shared/init-database-modal/init-database-modal.component';
import { DbService } from '../core/services/database/db.service';
import { IconComponent } from '../shared/icon/icon.component';

@Component({
  selector: 'app-pages',
  standalone: true,
  imports: [RouterModule, InitDatabaseModalComponent, IconComponent],
  templateUrl: './pages.component.html',
  styleUrl: './pages.component.css',
})
export class PagesComponent {
  constructor(private db: DbService) {}
  ngOnInit(): void {
    this.db.getProduct(1);
  }
}
