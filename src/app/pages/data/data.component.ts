import { Component } from '@angular/core';
import { IconComponent } from '../../shared/icon/icon.component';
import { DbService } from '../../core/services/database/db.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-data',
  standalone: true,
  imports: [IconComponent, FormsModule],
  templateUrl: './data.component.html',
  styleUrl: './data.component.css',
})
export class DataComponent {
  selectedFile: File | null = null;
  constructor(private dbService: DbService) {}

  exportDatabase() {
    this.dbService.exportToCSV();
  }

  onFileSelected(event: any): void {
    const file: File = event.target.files[0];
    if (file) {
      this.selectedFile = file;
    } else {
      this.selectedFile = null;
    }
  }

  importData(): void {
    if (this.selectedFile) {
      this.dbService.importFromExcel(this.selectedFile).subscribe(() => {
        console.log('Datos importados exitosamente');
      });
    } else {
      console.error('No se ha seleccionado ningún archivo');
    }
  }
}
