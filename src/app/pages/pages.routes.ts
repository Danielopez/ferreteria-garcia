import { Routes } from '@angular/router';
import { SalesComponent } from './sales/sales.component';
import { PagesComponent } from './pages.component';
import { ManagerProductsComponent } from './manager-products/manager-products.component';
import { EditComponent } from './manager-products/edit/edit.component';
import { ListComponent } from './manager-products/list/list.component';
import { DataComponent } from './data/data.component';

export const PAGES_ROUTES: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        redirectTo: 'ventas',
        pathMatch: 'full',
      },
      {
        path: 'ventas',
        component: SalesComponent,
      },
      {
        path: 'productos',
        component: ManagerProductsComponent,
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full',
          },
          {
            path: 'list',
            component: ListComponent,
          },
          {
            path: 'edit',
            component: EditComponent,
          },
        ],
      },
      {
        path: 'data',
        component: DataComponent,
      },
    ],
  },
];
