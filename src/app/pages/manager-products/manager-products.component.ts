import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconComponent } from '../../shared/icon/icon.component';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { ModalService } from '../../core/services/modals/modal.service';
import { DbService } from '../../core/services/database/db.service';
import { Product } from '../../core/services/database/class/product.class';
import { TableProductsComponent } from './list/table-products/table-products.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-manager-products',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './manager-products.component.html',
  styleUrl: './manager-products.component.css',
})
export class ManagerProductsComponent {}
