import { Component, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { DbService } from '../../../core/services/database/db.service';
import { Product } from '../../../core/services/database/class/product.class';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoadingModalComponent } from '../../../shared/loading-modal/loading-modal.component';
import { ModalService } from '../../../core/services/modals/modal.service';
import { Modals } from '../../../core/enums/modals.enum';
import { formatDate } from '../../../utils/utils';

@Component({
  selector: 'app-edit',
  standalone: true,
  imports: [CommonModule, FormsModule, LoadingModalComponent, RouterModule],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.css',
})
export class EditComponent {
  id: number | undefined;
  product: Product | undefined;

  constructor(
    private route: ActivatedRoute,
    private dbService: DbService,
    private modalService: ModalService,
    private router: Router
  ) {
    this.id = Number(this.route.snapshot.queryParamMap.get('id'));
  }
  get costoNeto() {
    if (this.product) {
      const descount = this.product.COSTOL * ((this.product.DC || 0) / 100);
      const increment = this.product.COSTOL * ((this.product.DI || 0) / 100);
      let costo = this.product.COSTOL - descount + increment;
      costo = Number(costo.toFixed(2));
      return costo;
    }
    return 0;
  }

  get price_1() {
    if (this.product) {
      const price = (this.product.COSTO || 0) * this.product.PRC1;
      const priceFixed = Number(price.toFixed(2));
      return priceFixed;
    }
    return 0;
  }
  get price_2() {
    if (this.product) {
      const price = (this.product.COSTO || 0) * (this.product.PRC2 || 0);
      const priceFixed = Number(price.toFixed(2));
      return priceFixed;
    }
    return 0;
  }
  get price_3() {
    if (this.product) {
      const price = (this.product.COSTO || 0) * (this.product.PRC3 || 0);
      const priceFixed = Number(price.toFixed(2));
      return priceFixed;
    }
    return 0;
  }
  get validate() {
    if (this.product) {
      if (this.product.ARTICULO == '') {
        return true;
      }
      const numericProperties = [
        this.product.CLAV,
        this.product.COSTOL,
        this.product.DC,
        this.product.DI,
        this.product.PRC1,
        this.product.PRC2,
        this.product.PRC3,
        this.product.COSTO,
        this.product.PRECIO1,
        this.product.PRECIO2,
        this.product.PRECIO3,
      ];

      return numericProperties.some(
        (prop) => prop === null || prop === undefined || prop < 0
      );
    }

    return true;
  }

  get update() {
    const date = formatDate(this.product?.UPDATE);
    return date;
  }

  ngOnInit(): void {
    if (this.id) {
      this.dbService.getProduct(this.id).subscribe((data) => {
        this.product = data;
      });
    }
  }
  updateCosto() {
    if (this.product) {
      this.product.COSTO = this.costoNeto;
      this.updatePrices();
    }
  }
  updatePrices() {
    if (this.product) {
      this.product.PRECIO1 = this.price_1;
      this.product.PRECIO2 = this.price_2;
      this.product.PRECIO3 = this.price_3;
    }
  }
  updateProduct() {
    console.log(this.product, this.validate);

    if (this.product && !this.validate) {
      const clav = this.product.CLAV;
      if (clav) {
        this.modalService.openModal(Modals.LOADING);
        delete this.product.CLAV;
        this.dbService.updateProduct(clav, this.product).subscribe(() => {
          this.modalService.closeModal(Modals.LOADING);
          this.router.navigate(['/productos/list'], {
            queryParams: { id: clav },
          });
        });
      }
    }
  }
}
