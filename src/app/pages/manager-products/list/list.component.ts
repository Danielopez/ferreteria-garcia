import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Product } from '../../../core/services/database/class/product.class';
import { ModalService } from '../../../core/services/modals/modal.service';
import { DbService } from '../../../core/services/database/db.service';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { TableProductsComponent } from './table-products/table-products.component';
import { IconComponent } from '../../../shared/icon/icon.component';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertComponent } from '../../../shared/alert/alert.component';
import { SearchManager } from '../../../core/class/search-manager.class';

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    IconComponent,
    TableProductsComponent,
    AlertComponent,
  ],
  templateUrl: './list.component.html',
  styleUrl: './list.component.css',
})
export class ListComponent {
  @ViewChild('searchInput') searchInput!: ElementRef<HTMLInputElement>;
  search = new FormControl('');
  products: Product[] | undefined;
  index: number = 0;
  id: number | undefined;
  local = new SearchManager();

  constructor(
    private modalService: ModalService,
    private db: DbService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.id = Number(this.route.snapshot.queryParamMap.get('id'));
  }
  ngOnInit(): void {
    this.search.valueChanges
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe((query) => {
        if (query) {
          this.local.setSearch = query;
          this.db.searchProductsFull(query).subscribe((data) => {
            this.products = data;
            setTimeout(() => {
              this.index = this.local.getIndex;
            }, 400);
          });
        }
      });
    if (this.id) {
      this.db.getProduct(this.id).subscribe((data) => {
        if (data) {
          this.search.setValue(data.ARTICULO);
        }
      });
    }
    this.search.setValue(this.local.getSearch);
    setTimeout(() => {
      this.index = this.local.getIndex;
    }, 400);

    setTimeout(() => {
      this.searchInput.nativeElement.focus();
      this.searchInput.nativeElement.select();
    }, 0);
  }
  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (this.products && this.products.length > 0) {
      if (event.key === 'ArrowUp') {
        if (this.index > 0) {
          this.index--;
          this.local.setIndex = this.index;
        }
        setTimeout(() => {
          const inputElement = this.searchInput.nativeElement;
          inputElement.setSelectionRange(
            inputElement.value.length,
            inputElement.value.length
          );
        }, 0);
      } else if (event.key === 'ArrowDown') {
        if (this.index < this.products.length - 1) {
          this.index++;
          this.local.setIndex = this.index;
        }
      } else if (event.key === 'Enter') {
        const product = this.products[this.index];
        if (product) {
          this.router.navigate(['/productos/edit'], {
            queryParams: { id: product.CLAV },
          });
        }
      }
    }
  }
}
