import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { Product } from '../../../../core/services/database/class/product.class';
import { formatDate } from '../../../../utils/utils';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-table-products',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './table-products.component.html',
  styleUrl: './table-products.component.css',
})
export class TableProductsComponent {
  @ViewChildren('productRow') productRows!: QueryList<ElementRef>;
  @Input()
  products: Product[] | undefined;
  @Input()
  index: number = 0;
  @Output()
  clickProductEvent = new EventEmitter<number>();

  headers: string[] = [
    'CLAVE',
    'ARTICULO',
    'PROVEDOR',
    'COSTOL',
    'COSTO',
    'PRECIO1',
    'PRECIO2',
    'PRECIO3',
    'ACTUALIZACION',
  ];

  formatDate(date: Date | undefined) {
    return formatDate(date);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['index']) {
      this.scrollToHighlighted();
    }
  }

  clickProduct(index: number) {
    this.clickProductEvent.emit(index);
  }

  scrollToHighlighted() {
    if (this.products && this.productRows) {
      const highlightedRow = this.productRows.toArray()[this.index];
      if (highlightedRow) {
        highlightedRow.nativeElement.scrollIntoView({
          block: 'center',
        });
      }
    }
  }
}
