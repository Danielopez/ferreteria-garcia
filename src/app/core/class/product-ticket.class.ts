import { Product } from '../services/database/class/product.class';

export class ProductTicket {
  product: Product;
  quantity: number;
  price: number;
  typePrice: number;
  constructor(product: Product) {
    this.product = product;
    this.quantity = 1;
    this.typePrice = 1;
    this.price = product.PRECIO1 || 0 * this.quantity;
  }
  setPrice(price: number) {
    this.price = price;
  }
  setQuantity(quantity: number) {
    this.quantity = quantity;
  }
}
