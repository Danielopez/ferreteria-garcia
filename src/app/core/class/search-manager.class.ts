export class SearchManager {
  private search: string;
  private index: number;
  constructor() {
    const searchTmp = this.getSearch;
    if (!searchTmp && searchTmp != '') {
      this.setSearch = '';
    }
    this.search = this.getSearch;
    const indexTmp = this.getIndex;
    if (!indexTmp && indexTmp != 0) {
      this.setIndex = 0;
    }
    this.index = this.getIndex;
  }
  get getSearch() {
    return localStorage.getItem('search-m') || '';
  }
  set setSearch(value: string) {
    if (this.search != value) {
      this.setIndex = 0;
    }
    localStorage.setItem('search-m', value);
    this.search = this.getSearch;
  }
  get getIndex() {
    return Number(localStorage.getItem('index-m')) || 0;
  }
  set setIndex(value: number) {
    localStorage.setItem('index-m', `${value}`);
    this.index = this.getIndex;
  }
}
