import { ProductTicket } from '../../../class/product-ticket.class';

export class Ticket {
  products: ProductTicket[];
  total: number;
  constructor() {
    this.products = [];
    this.total = 0;
  }
  setTotal() {
    this.products.map((product) => {
      this.total = this.total + product.price * product.quantity;
    });
  }
  addProduct(product: ProductTicket) {
    this.products.push(product);
    this.setTotal();
  }
  deleteProduct(index: number) {
    if (index > -1 && index < this.products.length) {
      this.products.splice(index, 1);
      this.setTotal();
    }
  }
  resetTicket() {
    this.products = [];
    this.total = 0;
  }
}
