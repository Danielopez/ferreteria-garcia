import { Injectable } from '@angular/core';
import { ProductTicket } from '../../class/product-ticket.class';
import { BehaviorSubject } from 'rxjs';
import { Ticket } from './class/ticket.class';

@Injectable({
  providedIn: 'root',
})
export class TicketService {
  ticket: Ticket;
  private _products: BehaviorSubject<Ticket>;
  constructor() {
    this.ticket = new Ticket();
    this._products = new BehaviorSubject<Ticket>(this.ticket);
  }

  get ticketBO() {
    return this._products.asObservable();
  }

  addProduct(product: ProductTicket) {
    this.ticket.addProduct(product);
    this._products.next(this.ticket);
  }
  deleteProduct(index: number) {
    this.ticket.deleteProduct(index);
    this._products.next(this.ticket);
  }
  resetTicket() {
    this.ticket.resetTicket();
    this._products.next(this.ticket);
  }
}
