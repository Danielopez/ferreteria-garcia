export interface LoadInterface {
  open: boolean;
  finish: boolean;
}
