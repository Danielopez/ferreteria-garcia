export interface ProductInteface {
  CLAV?: number;
  ARTICULO: string;
  COSTOL: number;
  PRC1: number;
  DC?: number;
  DI?: number;
  COSTO?: number;
  PRC2?: number;
  PRC3?: number;
  PRECIO1?: number;
  PRECIO2?: number;
  PRECIO3?: number;
  U1?: string;
  U2?: string;
  U3?: string;
  CLPR?: string;
  CREATE?: Date;
  UPDATE?: Date;
}
