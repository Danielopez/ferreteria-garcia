import { ProductInteface } from '../interfaces/product.interface';

export class Product {
  CLAV?: number;
  ARTICULO: string;
  COSTOL: number;
  DC?: number = 0;
  DI?: number = 0;
  PRC1: number;
  PRC2?: number = 0;
  PRC3?: number = 0;
  COSTO?: number = 0;
  U1?: string = 'SIN UNIDAD';
  U2?: string = 'SIN UNIDAD';
  U3?: string = 'SIN UNIDAD';
  PRECIO1?: number;
  PRECIO2?: number;
  PRECIO3?: number;
  CREATE?: Date; // Valor por defecto: fecha de creación
  UPDATE?: Date; // Valor por defecto: fecha de creación cuando se crea el dato
  CLPR?: string = 'SIN PROVEDOR';

  constructor(data: ProductInteface) {
    if (data.CLAV) this.CLAV = Number(data.CLAV);
    this.ARTICULO = data.ARTICULO;
    this.COSTOL = Number(data.COSTOL);
    this.PRC1 = Number(data.PRC1);
    if (data.DC) this.DC = Number(data.DC);
    if (data.DI) this.DI = Number(data.DI);
    this.COSTO = data.COSTOL - (data.DC || 0) + (data.DI || 0);
    if (data.COSTO) {
      this.COSTO = Number(data.COSTO);
    }
    if (data.PRC2) this.PRC2 = Number(data.PRC2);
    if (data.PRC3) this.PRC3 = Number(data.PRC3);
    if (data.U1) this.U1 = data.U1;
    if (data.U2) this.U2 = data.U2;
    if (data.U3) this.U3 = data.U3;
    if (data.CLPR) this.CLPR = data.CLPR;
    this.PRECIO1 = this.setPrice(1);
    if (this.PRECIO1) {
      this.PRECIO1 = Number(data.PRECIO1);
    }
    this.PRECIO2 = this.setPrice(2);
    if (this.PRECIO1) {
      this.PRECIO1 = Number(data.PRECIO1);
    }
    this.PRECIO3 = this.setPrice(3);
    if (this.PRECIO1) {
      this.PRECIO1 = Number(data.PRECIO1);
    }
    this.CREATE = new Date();
    if (data.CREATE) {
      this.CREATE = data.CREATE;
    }
    this.UPDATE = new Date();
    if (data.UPDATE) {
      this.UPDATE = data.UPDATE;
    }
  }

  private fixNumber(number: number) {
    return Number(number.toFixed(2));
  }
  private setPrice(typePrice: number) {
    switch (typePrice) {
      case 1:
        return this.fixNumber((this.COSTO || 0) * this.PRC1);
      case 2:
        return this.fixNumber((this.COSTO || 0) * (this.PRC2 || 0));
      case 3:
        return this.fixNumber((this.COSTO || 0) * (this.PRC3 || 0));
      default:
        return 0;
    }
  }
}
