import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import { Product } from './class/product.class';
import { from, Observable } from 'rxjs';
import { ModalService } from '../modals/modal.service';
import { Modals } from '../../enums/modals.enum';
import * as XLSX from 'xlsx';
import { getCurrentDate } from '../../../utils/utils';

@Injectable({
  providedIn: 'root',
})
export class DbService extends Dexie {
  private products: Dexie.Table<Product, number>;
  constructor(private modalService: ModalService) {
    super('Ferreteria-Garcia');
    this.version(2).stores({
      products:
        'CLAV, ARTICULO, COSTOL, DC, DI, PRC1, PRC2, PRC3, U1, U2, U3, PRECIO1, PRECIO2, PRECIO3, CREATE, UPDATE, CLPR',
    });
    this.products = this.table('products');
    this.on('populate', () => {
      this.loadInitialData();
    });
  }
  private loadInitialData(): void {
    this.modalService.openModal(Modals.INIT_DATA);
    fetch('./assets/data/base.json')
      .then((response) => response.json())
      .then((json) => {
        for (let data of json) {
          const product = new Product(data);
          this.products.add(product);
        }
        setTimeout(() => {
          this.modalService.closeModal(Modals.INIT_DATA);
        }, 20000);
      });
  }
  getProduct(CLAV: number): Observable<Product | undefined> {
    return from(this.products.get(CLAV));
  }
  searchProducts(pattern: string): Observable<Product[]> {
    return from(
      this.products
        .where('ARTICULO')
        .startsWithAnyOfIgnoreCase(pattern)
        .toArray()
    );
  }
  searchProductsFull(pattern: string): Observable<Product[]> {
    return from(
      Promise.all([
        this.products
          .where('ARTICULO')
          .startsWithAnyOfIgnoreCase(pattern)
          .toArray(),
        this.products
          .where('CLPR')
          .startsWithAnyOfIgnoreCase(pattern)
          .toArray(),
      ]).then(([articuloResults, clprResults]) => {
        const combinedResults = [...articuloResults, ...clprResults];
        const uniqueResults = Array.from(
          new Set(combinedResults.map((item) => item?.CLAV))
        ).map((id) => combinedResults.find((item) => item?.CLAV === id));
        return uniqueResults.filter(
          (item): item is Product => item !== undefined
        );
      })
    );
  }
  getAllProducts(): Observable<Product[]> {
    return from(this.products.toArray());
  }
  updateProduct(CLAV: number, updatedProduct: Product): Observable<any> {
    updatedProduct.UPDATE = new Date();
    return from(this.products.update(CLAV, updatedProduct));
  }
  deleteProduct(CLAV: number): Observable<any> {
    return from(this.products.delete(CLAV));
  }
  exportToCSV(): void {
    const date = getCurrentDate();
    this.getAllProducts().subscribe((products) => {
      const name = 'Ferreteria-Garcia' + date;
      this.exportAsExcelFile(products, name);
    });
  }
  private exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ['data'],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'array',
    });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    const url = window.URL.createObjectURL(data);
    const link = document.createElement('a');
    link.href = url;
    link.download = `${fileName}.xlsx`;
    link.click();
  }
  importFromExcel(file: File) {
    return from(
      file.arrayBuffer().then((buffer) => {
        const workbook = XLSX.read(buffer, { type: 'array' });
        const worksheet = workbook.Sheets[workbook.SheetNames[0]];
        const json: any[] = XLSX.utils.sheet_to_json(worksheet);
        const products: Product[] = json.map((item) => {
          return {
            ...item,
            CREATE: this.excelDateToJSDate(item.CREATE),
            UPDATE: this.excelDateToJSDate(item.UPDATE),
          };
        });

        return this.delete().then(() => {
          return this.open().then(() => {
            return this.products.clear().then(() => {
              return this.products.bulkAdd(products);
            });
          });
        });
      })
    );
  }
  private excelDateToJSDate = (serial: number) => {
    const utc_days = Math.floor(serial - 25569);
    const utc_value = utc_days * 86400;
    const date_info = new Date(utc_value * 1000);

    const fractional_day = serial - Math.floor(serial) + 0.0000001;
    let total_seconds = Math.floor(86400 * fractional_day);

    const seconds = total_seconds % 60;
    total_seconds -= seconds;

    const hours = Math.floor(total_seconds / (60 * 60));
    const minutes = Math.floor(total_seconds / 60) % 60;

    let date = new Date(
      date_info.getFullYear(),
      date_info.getMonth(),
      date_info.getDate(),
      hours,
      minutes,
      seconds
    );
    date.setDate(date.getDate() + 1);

    return date;
  };
}
