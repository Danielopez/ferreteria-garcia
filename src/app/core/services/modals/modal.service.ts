import { Injectable } from '@angular/core';
import { Modals } from '../../enums/modals.enum';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  modals: boolean[];
  private _modals: BehaviorSubject<boolean[]>;
  constructor() {
    const enumSize = Object.keys(Modals).length / 2;
    this.modals = new Array(enumSize).fill(false);
    this._modals = new BehaviorSubject<boolean[]>(this.modals);
  }
  get modalsBO() {
    return this._modals.asObservable();
  }
  openModal(modal: Modals) {
    this.modals[modal] = true;
    this._modals.next(this.modals);
  }
  closeModal(modal: Modals) {
    this.modals[modal] = false;
    this._modals.next(this.modals);
  }
  resetModals() {
    const enumSize = Object.keys(Modals).length / 2;
    this.modals = new Array(enumSize).fill(false);
  }
}
