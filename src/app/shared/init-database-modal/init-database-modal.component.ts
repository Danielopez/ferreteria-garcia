import { Component } from '@angular/core';
import { IconComponent } from '../icon/icon.component';
import { ModalService } from '../../core/services/modals/modal.service';
import { Modals } from '../../core/enums/modals.enum';

@Component({
  selector: 'app-init-database-modal',
  standalone: true,
  imports: [IconComponent],
  templateUrl: './init-database-modal.component.html',
  styleUrl: './init-database-modal.component.css',
})
export class InitDatabaseModalComponent {
  open: boolean = false;
  constructor(private modalService: ModalService) {}
  ngOnInit(): void {
    this.modalService.modalsBO.subscribe((data) => {
      this.open = data[Modals.INIT_DATA];
    });
  }
  closeModal() {
    this.modalService.closeModal(Modals.INIT_DATA);
  }
}
