import { Component, Input } from '@angular/core';
import { ModalService } from '../../core/services/modals/modal.service';
import { Modals } from '../../core/enums/modals.enum';

@Component({
  selector: 'app-loading-modal',
  standalone: true,
  imports: [],
  templateUrl: './loading-modal.component.html',
  styleUrl: './loading-modal.component.css',
})
export class LoadingModalComponent {
  @Input()
  open: boolean = false;
  constructor(private modalService: ModalService) {}
  ngOnInit(): void {
    this.modalService.modalsBO.subscribe((data) => {
      this.open = data[Modals.LOADING];
    });
  }
}
